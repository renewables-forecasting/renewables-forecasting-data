# renewables-forecasting-data

Repository containing datasets used in the [*renewables-forecasting* project](https://gitlab.com/renewables-forecasting/renewables-forecasting/).

## Format

Unless otherwise stated, CSV datasets use UTF-8 character encoding, `.` as decimals, and `,` as the separator.

## Data

### Geography

Type | Description | License
--- | --- | ---
Polygons at bidding zone level for Germany, Austria, Switzerland, Czech Republic, Denmark, France, Luxembourg, the Netherlands, Poland, and Sweden | [README](geography/README.md#bidding-zones) | [README](geography/README.md#license-and-copyright-information)
Polygons at country level for Germany, Austria, Switzerland, Czech Republic, Denmark, France, Luxembourg, the Netherlands, Poland, and Sweden | [README](geography/README.md#countries) | [README](geography/README.md#license-and-copyright-information)
National postcodes for Germany | [README](geography/README.md#postal-codes) | [README](geography/README.md#license)

### Meteorology

Type | Description | License
--- | --- | ---
List of German meteorological stations with hourly data between 01.01.2018 and 01.07.2018 | [README](meteorology/README.md#list-of-stations) | [README](meteorology/README.md#license-and-copyright)
Historical hourly station observations of wind speed and wind direction for Germany; data between 01.01.2018 and 01.07.2018 | [README](meteorology/README.md#wind) | [README](meteorology/README.md#license-and-copyright)

### Power

Type | Description | License
--- | --- | ---
Installed renewable energy generators in Germany in 2018 | [README](power/README.md#renewable-power-generators) |
Installed generation capacity in the DE-AT-LU bidding zone between 01.01.2018 and 01.07.2018 | [README](power/README.md#installed-generation-capacity) | [README](power/README.md#entso-e-transparency-platform-data)
Generation and generation forecasts in the DE-AT-LU bidding zone between 01.01.2018 and 01.07.2018 | [README](power/README.md#generation) | [README](power/README.md#entso-e-transparency-platform-data)
Load and load forecasts in the DE-AT-LU bidding zone between 01.01.2018 and 01.07.2018 | [README](power/README.md#load) | [README](power/README.md#entso-e-transparency-platform-data)
Day-ahead electricity market prices in the DE-AT-LU bidding zone between 01.01.2018 and 01.07.2018 | [README](power/README.md#prices) | [README](power/README.md#entso-e-transparency-platform-data)
